from web3 import Web3
from walletconstructor.security import Security
from walletconstructor.infos.config import PRICE_ETH, ROUDING_PRECISION
from decimal import Decimal, ROUND_DOWN
import requests


class Infos(dict):
    ether_price = PRICE_ETH

    def __init__(
            self,
            web3:Web3,
            security:Security,
            *args,
            **kwargs
    ) -> None:
        self._web3 = web3
        self._security = security
        super(*args, **kwargs)
        self._initialisation_eth()
        self.update_infos()

    def _initialisation_eth(self) -> None:
        self['eth'] = {}
        self['eth']['sold'] = {}
        self['eth']['balance'] = Decimal('0')
        return None
    
    def __balance_eth(self) -> None:
        try:
            balance_wei = self._web3.eth.get_balance(self._security.addr_ethereum)
            balance_eth = Decimal(self._web3.from_wei(balance_wei, 'ether')).quantize(
                ROUDING_PRECISION, rounding=ROUND_DOWN)
            self['eth']['balance'] = balance_eth
            return None
        except Exception as e:
            raise Exception("Error balance getting") from e
        
    def __sold_eth(self) -> None:
        try:
            ether_balance = self['eth']['balance']
            ether_price = self.ether_price
            for k, v in ether_price.items():
                decimal_value = Decimal(v)
                self['eth']['sold'][k] = Decimal((ether_balance*decimal_value)).quantize(ROUDING_PRECISION, rounding=ROUND_DOWN)
            return None
        except Exception as e:
            raise Exception("Error sold eth") from e
        
    def __init_eth_infos(self) -> None:
        self.__balance_eth()
        self.__sold_eth()

    def update_infos(self) -> None:
        self.__init_eth_infos()