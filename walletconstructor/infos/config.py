import requests
import threading
import time
from decimal import Decimal

def _get_ether_price() -> dict:
    url = "https://api.coingecko.com/api/v3/coins/ethereum"
    try:
        response = requests.get(url)
        data = response.json()
        price_eth = {
            'USD': data['market_data']['current_price']['usd'],
            'EUR': data['market_data']['current_price']['eur'],
            'BTC': data['market_data']['current_price']['btc']
        }
        return price_eth
    except requests.RequestException as e:
        raise Exception("Error getting price ether") from e

def _refresh_price():
    global PRICE_ETH
    while True:
        PRICE_ETH = _get_ether_price()
        time.sleep(600)

def start_price_refresh_thread():
    thread = threading.Thread(target=_refresh_price)
    thread.daemon = True
    thread.start()

global PRICE_ETH
PRICE_ETH = _get_ether_price()
ROUDING_PRECISION = Decimal('0.001')
start_price_refresh_thread()